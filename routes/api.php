<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['namespace' => 'API'], function () {
    Route::get('/', function () {
        return res('API Server is running...');
    });

    Route::get('/unauthenticated', function () {
        return res('Unauthenticated', null, 401);
    })->name('unauthenticated');

    Route::group(['prefix' => 'auth', 'namespace' => 'Auth'], function () {
        Route::post('/request-otp', 'AuthController@requestOTP');
        Route::post('/verify-otp', 'AuthController@verifyOTP');
        Route::post('/driver-request-otp', 'AuthController@driverRequestOTP');
        Route::post('/admin-request-otp', 'AuthController@adminRequestOTP');
    });

    Route::group(['prefix' => 'driver', 'namespace' => 'Driver'], function () {
        Route::post('/register', 'DriverController@register');
        Route::post('/verify-registration-otp', 'DriverController@verifyRegistrationOTP');
    });

    Route::group(['middleware' => 'auth:api'], function () {
        Route::get('/validate-token', function () {
            return res('Success');
        });

        Route::group(['prefix' => 'auth', 'namespace' => 'Auth'], function () {
            Route::post('/refresh-token', 'AuthController@refreshToken');
        });

        Route::group(['prefix' => 'clothing', 'namespace' => 'Clothing'], function () {
            Route::post('/types', 'ClothingController@types');
            Route::post('/groups', 'ClothingController@groups');
            Route::post('/clothes', 'ClothingController@clothes');
            Route::post('/delivery-types', 'ClothingController@deliveryTypes');
        });

        Route::group(['prefix' => 'settings', 'namespace' => 'Settings'], function () {
            Route::post('/get', 'SettingsController@get');
            Route::post('/update', 'SettingsController@update');
        });

        Route::group(['prefix' => 'address', 'namespace' => 'Address'], function () {
            Route::post('/list', 'AddressController@list');
            Route::post('/add', 'AddressController@add');
            Route::post('/update', 'AddressController@update');
            Route::post('/remove', 'AddressController@remove');
            Route::post('/get-default', 'AddressController@getDefault');
        });

        Route::group(['prefix' => 'preference', 'namespace' => 'Preference'], function () {
            Route::post('/get', 'PreferenceController@get');
            Route::post('/update', 'PreferenceController@update');
        });

        Route::group(['prefix' => 'cart', 'namespace' => 'Cart'], function () {
            Route::post('/my-list', 'CartController@myList');
            Route::post('/list', 'CartController@list');
            Route::post('/add', 'CartController@add');
            Route::post('/update-payment-status', 'CartController@updatePaymentStatus');
            Route::post('/update-order-status', 'CartController@updateOrderStatus');
            Route::post('/transit', 'CartController@transit');
        });

        Route::group(['prefix' => 'dashboard', 'namespace' => 'Dashboard'], function () {
            Route::post('/summary', 'DashboardController@summary');
        });

        Route::group(['prefix' => 'profile', 'namespace' => 'Profile'], function () {
            Route::post('/fetch', 'ProfileController@fetch');
            Route::post('/update', 'ProfileController@update');
            Route::post('/change-avatar', 'ProfileController@changeAvatar');
            Route::post('/change-mobile', 'ProfileController@changeMobile');
        });

        Route::group(['prefix' => 'user', 'namespace' => 'User'], function () {
            Route::post('/list', 'UserController@list');
            Route::post('/details', 'UserController@details');
            Route::post('/update', 'UserController@update');
            Route::post('/change-avatar', 'UserController@changeAvatar');
            Route::post('/change-mobile', 'UserController@changeMobile');
            Route::post('/toggle-admin-role', 'UserController@toggleAdminRole');
            Route::post('/toggle-admin-status', 'UserController@toggleAdminStatus');
            Route::post('/toggle-driver-status', 'UserController@toggleDriverStatus');
        });

        Route::group(['prefix' => 'driver', 'namespace' => 'Driver'], function () {
            Route::post('/upload-files', 'DriverController@uploadFiles');
            Route::post('/get-status', 'DriverController@getStatus');
        });
    });
});

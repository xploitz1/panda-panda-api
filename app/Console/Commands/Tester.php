<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class Tester extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'tester:start';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'General Tester';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $company_percentage = 0.55;
        $room_percentage = 0.05;
        $user_percentage = 0.40;

        $principal = 1000000;
        $bonus = 250000;
        $company = 0;
        $room = 0;
        $user = 0;
        $p_room = 0;
        $p_company = 0;
        $rotation = 0;
        $principal += $bonus;
        while ($principal > 0) {
            $company += $principal * $company_percentage;
            $room += $principal * $room_percentage;
            $user += $principal * $user_percentage;
            $principal = $principal * $user_percentage;
            $rotation++;
        }
        $p_company = (($company - $bonus) / 50000) * 1500;
        $p_room = ($room / 50000) * 1500;
        dump(compact('principal', 'company', 'room', 'p_company', 'p_room', 'rotation'));
    }
}

<?php

namespace App\Repositories;

use App\Http\Resources\AddressResource;
use App\Interfaces\AddressInterface;
use App\Models\Address;
use Illuminate\Support\Facades\Validator;

class AddressRepository implements AddressInterface
{
    public function list($req)
    {
        $a = Address::where('user_id', auth()->id())->where('status', 1)->orderBy('default', 'desc')->get();
        $r = AddressResource::collection($a);
        return res('Success', $r);
    }

    public function add($req)
    {
        $validator = Validator::make($req->all(), [
            'lat' => 'required',
            'lng' => 'required',
            'street' => 'required',
            'building' => 'required',
            'default' => 'required',
        ]);
        if ($validator->fails()) {
            return res('Failed', $validator->errors(), 412);
        }

        $a = new Address;
        $a->user_id = auth()->id();
        $a->lat = $req->lat;
        $a->lng = $req->lng;
        $a->street = $req->street;
        $a->building = $req->building;
        $a->floor = $req->has('floor') ? $req->floor : '';
        $a->other = $req->has('other') ? $req->other : '';
        $a->mobile = $req->has('mobile') ? $req->mobile : '';

        $oa = Address::where('user_id', auth()->id())->where('default', 1)->where('status', 1)->first();
        if ($oa == null) {
            $a->default = 1;
        }
        $a->save();

        return res('Success');
    }

    public function update($req)
    {
        $validator = Validator::make($req->all(), [
            'hashid' => 'required',
            'lat' => 'required',
            'lng' => 'required',
            'street' => 'required',
            'building' => 'required',
            'default' => 'required',
        ]);
        if ($validator->fails()) {
            return res('Failed', $validator->errors(), 412);
        }

        $id = decode($req->hashid, 'model');
        $a = Address::find($id);
        if ($a == null) {
            return res('Address not found', null, 400);
        }
        $a->lat = $req->lat;
        $a->lng = $req->lng;
        $a->street = $req->street;
        $a->building = $req->building;
        $a->floor = $req->has('floor') ? $req->floor : '';
        $a->other = $req->has('other') ? $req->other : '';
        $a->mobile = $req->has('mobile') ? $req->mobile : '';

        $oa = Address::where('user_id', auth()->id())->where('default', 1)->where('status', 1)->first();
        if ($oa != null) {
            if ($req->default == 1) {
                Address::where('id', '!=', $id)->where('user_id', auth()->id())->update(['default' => 0]);
                $a->default = 1;
            } else {
                $a->default = 0;
            }
        } else {
            $a->default = 1;
        }
        $a->save();

        return res('Success');
    }

    public function remove($req)
    {
        $validator = Validator::make($req->all(), [
            'hashid' => 'required',
        ]);
        if ($validator->fails()) {
            return res('Failed', $validator->errors(), 412);
        }

        $id = decode($req->hashid, 'model');
        $a = Address::find($id);
        if ($a == null) {
            return res('Address not found', null, 400);
        }

        $a->status = 0;
        $a->save();

        return res('Success');
    }

    public function getDefault($req)
    {
        $a = Address::where('user_id', auth()->id())->where('default', 1)->where('status', 1)->first();
        if ($a == null) return res('No default address', null);
        $r = new AddressResource($a);
        return res('Success', $r);
    }
}

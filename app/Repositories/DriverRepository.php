<?php

namespace App\Repositories;

use App\Models\DriverInfo;
use App\User;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class DriverRepository implements \App\Interfaces\DriverInterface
{

    public function register($req)
    {
        $v = Validator::make($req->all(), [
            'name' => 'required',
            'qid' => 'required',
            'email' => 'required',
            'mobile' => 'required',
        ]);
        if ($v->fails()) {
            return res('Failed', $v->errors(), 412);
        }

        $otp = (string)rand(1000, 9999);

        $u = User::where('mobile', $req->mobile)->first();
        if ($u == null) {
            $username = Str::snake($req->name);
            $have_match = true;
            $username_ext = '';
            while ($have_match) {
                $other_user = User::where('username', $username . $username_ext)->count();
                if ($other_user > 0) {
                    $username_ext = (string)rand(1, 999);
                } else {
                    $have_match = false;
                    $username .= $username_ext;
                }
            }

            $ou = User::where('email', $req->email)->first();
            if ($ou) {
                return res('Email is already used by other', null, 400);
            }

            $u = new User;
            $u->mobile = $req->mobile;
            $u->password = '';
            $u->name = $req->name;
            $u->username = $username;
            $u->email = $req->email;
            if ($req->has('base64') && strlen($req->base64) > 16) {
                $u->base64 = $req->base64;
                save_avatar($u->username, $req->base64);
            } else {
                $u->base64 = '';
            }
        }

        $u->otp = $otp;
        $u->save();

        $odi = DriverInfo::where('user_id', $u->id)->first();
        if ($odi == null) {
            $di = new DriverInfo;
            $di->user_id = $u->id;
            $di->qid_number = $req->qid;
            $di->save();
        }

        $sent = send_verification($u->id, $u->mobile, $otp, 'Verification code from Panda Laundry ');

        return res('Success', compact('otp', 'sent'));
    }

    public function verifyRegistrationOTP($req)
    {
        $validator = Validator::make($req->all(), [
            'otp' => 'required|min:4|max:4',
        ]);
        if ($validator->fails()) {
            return res('Failed', $validator->errors(), 412);
        }

        $u = User::where('otp', $req->otp)->first();
        if ($u == null) {
            return res('Invalid OTP', null, 400);
        }

        $u->otp = null;
        $u->driver = 1;
        $u->save();

        $data = generateToken($u);
        return res('Success', $data);
    }

    public function uploadFiles($req)
    {
        $u = User::find(auth()->id());
        $di = DriverInfo::where('user_id', $u->id)->first();
        if ($di == null) {
            return res('Unable to find driver info', null, 400);
        }

        if ($req->has('qid_front') || $req->has('qid_back')) {
            $validator = Validator::make($req->all(), [
                'qid_front' => 'required',
                'qid_back' => 'required',
            ]);
            if ($validator->fails()) {
                return res('Failed', $validator->errors(), 412);
            }

            $di->qid_front_base64 = 1;
            $di->qid_back_base64 = 1;
            combine_image('driver-document/' . $u->username, 'qid.jpg', [$req->qid_front, $req->qid_back]);
        }
        if ($req->has('qdl_front') || $req->has('qdl_back')) {
            $validator = Validator::make($req->all(), [
                'qdl_front' => 'required',
                'qdl_back' => 'required',
            ]);
            if ($validator->fails()) {
                return res('Failed', $validator->errors(), 412);
            }

            $di->qdl_front_base64 = 1;
            $di->qdl_back_base64 = 1;
            combine_image('driver-document/' . $u->username, 'qdl.jpg', [$req->qdl_front, $req->qdl_back]);
        }
        if ($req->has('cv')) {
            $di->cv_base64 = 1;
            combine_image('driver-document/' . $u->username, 'cv.jpg', $req->cv);
        }

        $di->save();

        return res('Success');
    }

    public function getStatus($req)
    {
        $id = auth()->id();
        $u = User::where('id', $id)->where('driver', 1)->first();
        if ($u == null) return res('User not found', null, 400);

        $data = [
            'passed' => true,
            'qid' => true,
            'qdl' => true,
            'cv' => true,
        ];
        if ($u->driver_info == null) {
            $data['passed'] = false;
            $data['qid'] = false;
            $data['qdl'] = false;
            $data['cv'] = false;
        } else {
            if ($u->driver_info->qid_front_base64 == 0 || $u->driver_info->qid_back_base64 == 0) {
                $data['passed'] = false;
                $data['qid'] = false;
            }
            if ($u->driver_info->qdl_front_base64 == 0 || $u->driver_info->qdl_back_base64 == 0) {
                $data['passed'] = false;
                $data['qdl'] = false;
            }
            if ($u->driver_info->cv_base64 == 0) {
                $data['passed'] = false;
                $data['cv'] = false;
            }
        }

        return res('Success', $data);
    }
}

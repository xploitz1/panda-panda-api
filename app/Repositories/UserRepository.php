<?php

namespace App\Repositories;

use App\Http\Resources\UserResource;
use App\User;
use Illuminate\Support\Facades\Validator;

class UserRepository implements \App\Interfaces\UserInterface
{

    public function list($req)
    {
        $u = User::where('id', '!=', auth()->id())->get();
        if ($req->has('filter')) {
            $filter = $req->filter;
            if ($filter == 'driver') {
                $u = User::where('id', '!=', auth()->id())->where('driver', 1)->get();
            }
            if (Filter == 'admin') {
                $u = User::where('id', '!=', auth()->id())->where('admin', 1)->get();
            }
        }

        $r = UserResource::collection($u);
        return res('Success', $r);
    }

    public function details($req)
    {
        $v = Validator::make($req->all(), [
            'user_id' => 'required',
        ]);
        if ($v->fails()) {
            return res('Failed', $v->errors(), 412);
        }

        $id = decode($req->user_id, 'model');
        $u = User::find($id);
        if ($u == null) {
            return res('User not found', null, 400);
        }

        $r = new UserResource($u);
        return res('Success', $r);
    }

    public function update($req)
    {
        $v = Validator::make($req->all(), [
            'user_id' => 'required',
            'name' => 'required',
        ]);
        if ($v->fails()) {
            return res('Failed', $v->errors(), 412);
        }

        $id = decode($req->user_id, 'model');
        $u = User::find($id);
        if ($u == null) {
            return res('User not found', null, 400);
        }

        $u->name = $req->name;
        $u->save();

        return res('Success');
    }

    public function changeAvatar($req)
    {
        $v = Validator::make($req->all(), [
            'user_id' => 'required',
            'base64' => 'required',
        ]);
        if ($v->fails()) {
            return res('Failed', $v->errors(), 412);
        }

        $id = decode($req->user_id, 'model');
        $u = User::find($id);
        if ($u == null) {
            return res('User not found', null, 400);
        }

        $u->base64 = $req->base64;
        $u->save();

        save_avatar($u->username, $u->base64);

        return res('Success');
    }

    public function changeMobile($req)
    {
        $v = Validator::make($req->all(), [
            'user_id' => 'required',
            'mobile' => 'required',
        ]);
        if ($v->fails()) {
            return res('Failed', $v->errors(), 412);
        }

        $id = decode($req->user_id, 'model');
        $ou = User::where('id', '!=', $id)->where('mobile', $req->mobile)->first();
        if ($ou != null) {
            return res('Mobile number is already used by other', null, 400);
        }

        $u = User::find($id);
        if ($u == null) {
            return res('User not found', null, 400);
        }

        $u->mobile = $req->mobile;
        $u->save();

        return res('Success');
    }

    public function toggleAdminRole($req)
    {
        $v = Validator::make($req->all(), [
            'user_id' => 'required',
        ]);
        if ($v->fails()) {
            return res('Failed', $v->errors(), 412);
        }

        $id = decode($req->user_id, 'model');
        $u = User::find($id);
        if ($u == null) {
            return res('User not found', null, 400);
        }

        $u->admin = $u->admin == 1 ? 0 : 1;
        $u->save();

        return res('Success');
    }

    public function toggleAdminStatus($req)
    {
        $v = Validator::make($req->all(), [
            'user_id' => 'required',
        ]);
        if ($v->fails()) {
            return res('Failed', $v->errors(), 412);
        }

        $id = decode($req->user_id, 'model');
        $u = User::where('id', $id)->where('admin', 1)->first();
        if ($u == null) {
            return res('User not found', null, 400);
        }

        $u->admin_status = $u->admin_status == 1 ? 0 : 1;
        $u->save();

        return res('Success');
    }

    public function toggleDriverStatus($req)
    {
        $v = Validator::make($req->all(), [
            'user_id' => 'required',
        ]);
        if ($v->fails()) {
            return res('Failed', $v->errors(), 412);
        }

        $id = decode($req->user_id, 'model');
        $u = User::where('id', $id)->where('driver', 1)->first();
        if ($u == null) {
            return res('User not found', null, 400);
        }

        $u->driver_status = $u->driver_status == 1 ? 0 : 1;
        $u->save();

        return res('Success');
    }
}

<?php

namespace App\Repositories;

use App\Http\Resources\CartResource;
use App\Interfaces\CartInterface;
use App\Models\Cart;
use App\Models\CartItem;
use App\User;
use Illuminate\Support\Facades\Validator;

class CartRepository implements CartInterface
{
    public function myList($req)
    {
        $c = Cart::where('user_id', auth()->id())->orderBy('created_at', 'desc')->get();
        $r = CartResource::collection($c);
        return res('Success', $r);
    }

    public function list($req)
    {
        $u = User::find(auth()->id());
        if ($u->driver == 1 && $u->driver_status == 1) {
            if ($req->has('driver_history')) {
                $c = Cart::where(function($q) use ($u) {
                    return $q->where('pickup_driver_id', $u->id)->orWhere('delivery_driver_id', $u->id);
                })->orderBy('created_at', 'desc')->get();
            } else {
                $c = Cart::whereIn('status', [1, 3])->orderBy('delivery_type', 'asc')->orderBy('created_at', 'desc')->get();
            }
        } else {
            $c = Cart::orderBy('created_at', 'desc')->get();
        }
        $r = CartResource::collection($c);
        return res('Success', $r);
    }

    public function add($req)
    {
        $validator = Validator::make($req->all(), [
            'address_id' => 'required',
            'sub_total' => 'required',
            'delivery_fee' => 'required',
            'delivery_type' => 'required',
            'payment_method' => 'required',
            'payment_status' => 'required',
            'items' => 'required',
        ]);
        if ($validator->fails()) {
            return res('Failed', $validator->errors(), 412);
        }

        if (count($req->items) <= 0) {
            return res('Items are required', null, 400);
        }

        $c = new Cart;
        $c->user_id = auth()->id();
        $c->address_id = decode($req->address_id, 'model');
        $c->sub_total = $req->sub_total;
        $c->delivery_fee = $req->delivery_fee;
        $c->delivery_type = $req->delivery_type;
        $c->payment_method = $req->payment_method;
        $c->payment_status = $req->payment_status;
        $c->notes = $req->has('notes') ? $req->notes : '';
        $c->save();

        foreach ($req->items as $item) {
            $cloth_id = decode($item['cloth_id'], 'model');
            $ci = new CartItem;
            $ci->cart_id = $c->id;
            $ci->cloth_id = $cloth_id;
            $ci->process_type = $item['process_type'];
            $ci->quantity = $item['quantity'];
            $ci->price = $item['price'];
            $ci->save();
        }

        return res('Success', encode($c->id, 'order'));
    }

    public function updatePaymentStatus($req)
    {
        $validator = Validator::make($req->all(), [
            'cart_id' => 'required',
            'payment_status' => 'required',
        ]);
        if ($validator->fails()) {
            return res('Failed', $validator->errors(), 412);
        }

        $cart_id = decode($req->cart_id, 'model');
        $c = Cart::find($cart_id);
        if ($c == null) {
            return res('Order not found');
        }

        $c->payment_status = $req->payment_status;
        $c->save();

        return res('Success');
    }

    public function updateOrderStatus($req)
    {
        $validator = Validator::make($req->all(), [
            'cart_id' => 'required',
            'status' => 'required',
        ]);
        if ($validator->fails()) {
            return res('Failed', $validator->errors(), 412);
        }

        $cart_id = decode($req->cart_id, 'model');
        $c = Cart::find($cart_id);
        if ($c == null) {
            return res('Order not found');
        }

        $c->status = $req->status;
        $c->save();

        return res('Success');
    }

    public function transit($req)
    {
        $validator = Validator::make($req->all(), [
            'order_number' => 'required',
            'action' => 'required',
        ]);
        if ($validator->fails()) {
            return res('Failed', $validator->errors(), 412);
        }
        $actions = ['pickup', 'deliver'];
        if (!in_array($req->action, $actions)) {
            return res('Invalid action', null, 400);
        }

        $u = User::where('id', auth()->id())->where('driver', 1)->where('driver_status', 1)->first();
        if ($u == null) {
            return res('You are not a driver', null, 400);
        }
        $cart_id = decode($req->order_number, 'order');
        $c = Cart::find($cart_id);
        if ($c == null) {
            return res('Order not found', null, 400);
        }

        if ($req->action == $actions[0]) {
            if ($c->status != 1) {
                return res('Order is not for pickup', null, 400);
            }
            if ($c->pickup_driver_id != 0 && $c->pickup_driver_id != auth()->id()) {
                return res('Already assigned to other driver', null, 400);
            }

            $c->pickup_driver_id = auth()->id();
            $c->save();
        } else {
            if ($c->status != 3) {
                return res('Order is not for delivery', null, 400);
            }
            if ($c->delivery_driver_id != 0 && $c->delivery_driver_id != auth()->id()) {
                return res('Already assigned to other driver', null, 400);
            }

            $c->delivery_driver_id = auth()->id();
            $c->save();
        }

        return res('Success');
    }
}

<?php

namespace App\Repositories;

use App\Models\Cart;
use App\User;

class DashboardRepository implements \App\Interfaces\DashboardInterface
{

    public function summary($req)
    {
        $scope = 'year';
        $orders = Cart::whereYear('created_at', date('Y'))->whereBetween('status', [1, 4])->count();
        $deliveries = Cart::whereYear('created_at', date('Y'))->where('status', 3)->count();
        $drivers = User::where('driver', 1)->where('driver_status', 1)->count();
        $users = User::where('driver', '!=', 1)->where('admin', '!=', 1)->count();

        return res('Success', compact('scope', 'orders', 'deliveries', 'drivers', 'users'));
    }
}

<?php

namespace App\Repositories;

use App\Http\Resources\UserResource;
use App\User;
use Illuminate\Support\Facades\Validator;

class ProfileRepository implements \App\Interfaces\ProfileInterface
{

    public function fetch($req)
    {
        $u = User::find(auth()->id());
        $r = new UserResource($u);
        return res('Success', $r);
    }

    public function update($req)
    {
        $v = Validator::make($req->all(), [
            'name' => 'required',
        ]);
        if ($v->fails()) {
            return res('Failed', $v->errors(), 412);
        }

        $u = User::find(auth()->id());
        $u->name = $req->name;
        $u->save();

        return res('Success');
    }

    public function changeAvatar($req)
    {
        $v = Validator::make($req->all(), [
            'base64' => 'required',
        ]);
        if ($v->fails()) {
            return res('Failed', $v->errors(), 412);
        }

        $u = User::find(auth()->id());
        $u->base64 = $req->base64;
        $u->save();

        save_avatar($u->username, $req->base64);

        return res('Success');
    }

    public function changeMobile($req)
    {
        $v = Validator::make($req->all(), [
            'mobile' => 'required',
        ]);
        if ($v->fails()) {
            return res('Failed', $v->errors(), 412);
        }

        $ou = User::where('id', '!=', auth()->id())->where('mobile', $req->mobile)->first();
        if ($ou != null) {
            return res('Mobile number is already used by other', null, 400);
        }

        $u = User::find(auth()->id());
        $u->mobile = $req->mobile;
        $u->save();

        return res('Success');
    }
}

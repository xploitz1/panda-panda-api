<?php

namespace App\Repositories;

use App\Interfaces\AuthInterface;
use App\User;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class AuthRepository implements AuthInterface
{
    public function requestOTP($req)
    {
        $validator = Validator::make($req->all(), [
            'name' => 'required',
            'mobile' => 'required',
        ]);
        if ($validator->fails()) {
            return res('Failed', $validator->errors(), 412);
        }

        $otp = (string)rand(1000, 9999);

        $u = User::where('mobile', $req->mobile)->first();
        if ($u == null) {
            $username = Str::snake($req->name);
            $have_match = true;
            $username_ext = '';
            while ($have_match) {
                $other_user = User::where('username', $username . $username_ext)->count();
                if ($other_user > 0) {
                    $username_ext = (string)rand(1, 999);
                } else {
                    $have_match = false;
                    $username .= $username_ext;
                }
            }

            $u = new User;
            $u->name = $req->name;
            $u->username = $username;
            $u->email = '';
            $u->password = '';
            $u->mobile = $req->mobile;
            if ($req->has('base64') && strlen($req->base64) > 16) {
                $u->base64 = $req->base64;
                save_avatar($u->username, $req->base64);
            } else {
                $u->base64 = '';
            }
        }
        $u->otp = $otp;
        $u->save();

        $sent = send_verification($u->id, $u->mobile, $otp, 'Verification code from Panda Laundry ');

        return res('Success', compact('otp', 'sent'));
    }

    public function verifyOTP($req)
    {
        $validator = Validator::make($req->all(), [
            'otp' => 'required|min:4|max:4',
        ]);
        if ($validator->fails()) {
            return res('Failed', $validator->errors(), 412);
        }

        $u = User::where('otp', $req->otp)->first();
        if ($u == null) {
            return res('Invalid OTP', null, 400);
        }

        $u->otp = null;
        $u->save();

        $data = generateToken($u);
        return res('Success', $data);
    }

    public function refreshToken($req)
    {
        $user = auth()->user();
        $u = User::find($user->id);
        if ($u == null) {
            return res('User not found', null, 412);
        }

        $data = generateToken($u);
        return res('Success', $data);
    }

    public function driverRequestOTP($req)
    {
        $validator = Validator::make($req->all(), [
            'mobile' => 'required',
        ]);
        if ($validator->fails()) {
            return res('Failed', $validator->errors(), 412);
        }

        $otp = (string)rand(1000, 9999);

        $u = User::where('mobile', $req->mobile)->where('driver', 1)->where('driver_status', 1)->first();
        if ($u == null) {
            return res('Failed! Not registered as driver', null, 400);
        }
        $u->otp = $otp;
        $u->save();

        $sent = send_verification($u->id, $u->mobile, $otp, 'Verification code from Panda Laundry Driver ');

        return res('Success', compact('otp', 'sent'));
    }

    public function adminRequestOTP($req)
    {
        $validator = Validator::make($req->all(), [
            'mobile' => 'required',
        ]);
        if ($validator->fails()) {
            return res('Failed', $validator->errors(), 412);
        }

        $otp = (string)rand(1000, 9999);

        $u = User::where('mobile', $req->mobile)->where('admin', 1)->where('admin_status', 1)->first();
        if ($u == null) {
            return res('Failed! Not registered an administrator', null, 400);
        }
        $u->otp = $otp;
        $u->save();

        $sent = send_verification($u->id, $u->mobile, $otp, 'Verification code from Panda Laundry Administrator ');

        return res('Success', compact('otp', 'sent'));
    }
}

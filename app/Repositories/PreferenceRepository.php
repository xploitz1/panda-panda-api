<?php

namespace App\Repositories;

use App\Http\Resources\PreferenceResource;
use App\Interfaces\PreferenceInterface;
use App\Models\Preference;
use Illuminate\Support\Facades\Validator;

class PreferenceRepository implements PreferenceInterface
{
    public function get($req)
    {
        $p = Preference::firstOrCreate(['user_id' => auth()->id()]);
        $r = new PreferenceResource($p);
        return res('Success', $r);
    }

    public function update($req)
    {
        $validator = Validator::make($req->all(), [
            'bleach' => 'required',
            'softener' => 'required',
            'perfume' => 'required',
            'ironing' => 'required',
            'starch' => 'required',
            'returned' => 'required',
        ]);
        if ($validator->fails()) {
            return res('Failed', $validator->errors(), 412);
        }

        $p = Preference::firstOrCreate(['user_id' => auth()->id()]);
        $p->bleach = $req->bleach;
        $p->softener = $req->softener;
        $p->perfume = $req->perfume;
        $p->ironing = $req->ironing;
        $p->starch = $req->starch;
        $p->returned = $req->returned;
        $p->save();

        return res('Success');
    }
}

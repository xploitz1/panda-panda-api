<?php

namespace App\Repositories;

use App\Http\Resources\SettingsResource;
use App\Interfaces\SettingsInterface;
use App\Models\Settings;
use Illuminate\Support\Facades\Validator;

class SettingsRepository implements SettingsInterface
{
    public function get($req)
    {
        $validator = Validator::make($req->all(), [
            'name' => 'required',
        ]);
        if ($validator->fails()) {
            return res('Failed', $validator->errors(), 412);
        }

        $s = Settings::where('name', $req->name)->first();
        if ($s == null) {
            return res('Settings not found', null, 400);
        }

        $r = new SettingsResource($s);
        return res('Success', $r);
    }

    public function update($req)
    {
        $validator = Validator::make($req->all(), [
            'name' => 'required',
            'value' => 'required',
        ]);
        if ($validator->fails()) {
            return res('Failed', $validator->errors(), 412);
        }

        $s = Settings::where('name', $req->name)->first();
        if ($s == null) {
            return res('Settings not found', null, 400);
        }

        $s->value = $req->value;
        $s->save();

        $r = new SettingsResource($s);
        return res('Success', $r);
    }
}

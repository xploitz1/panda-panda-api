<?php

namespace App\Repositories;

use App\Models\Cloth;
use App\Models\ClothingType;
use App\Models\DeliveryType;
use App\Interfaces\ClothingInterface;
use App\Http\Resources\ClothesResource;
use Illuminate\Support\Facades\Validator;
use App\Http\Resources\ClothingTypeResource;
use App\Http\Resources\DeliveryTypeResource;
use App\Http\Resources\ClothingGroupResource;
use App\Models\ClothingGroup;

class ClothingRepository implements ClothingInterface
{
    public function types($req)
    {
        $ct = ClothingType::where('status', 1)->get();
        $r = ClothingTypeResource::collection($ct);
        return res('Success', $r);
    }

    public function groups($req)
    {
        $cg = ClothingGroup::where('status', 1)->get();
        $r = ClothingGroupResource::collection($cg);
        return res('Success', $r);
    }

    public function clothes($req)
    {
        $validator = Validator::make($req->all(), [
            'type_id' => 'required',
            'group_id' => 'required',
        ]);
        if ($validator->fails()) {
            return res('Failed', $validator->errors(), 412);
        }

        $type_id = decode($req->type_id, 'model');
        $group_id = decode($req->group_id, 'model');

        $c = Cloth::where('status', 1)->where('type_id', $type_id)->where('group_id', $group_id)->get();
        $r = ClothesResource::collection($c);
        return res('Success', $r);
    }

    public function deliveryTypes($req)
    {
        $dt = DeliveryType::where('status', 1)->get();
        $r = DeliveryTypeResource::collection($dt);
        return res('Success', $r);
    }
}

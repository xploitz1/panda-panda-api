<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cloth extends Model
{
    public function prices()
    {
        return $this->hasMany(Pricing::class, 'cloth_id');
    }
}

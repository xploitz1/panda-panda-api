<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ClothingType extends Model
{
    public function items()
    {
        return $this->hasMany(Cloth::class, 'type_id');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CartItem extends Model
{
    public function cloth()
    {
        return $this->belongsTo(Cloth::class, 'cloth_id');
    }
}

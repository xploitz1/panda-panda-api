<?php

use App\Http\Resources\UserResource;
use App\Models\OauthAccessToken;
use App\Models\RejectedSMS;
use App\User;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;
use Intervention\Image\Facades\Image;
use Nexmo\Laravel\Facade\Nexmo;

function res(string $msg = 'success', $data = null, int $code = 200)
{
    return response()->json([
        'code' => $code,
        'message' => $msg,
        'data' => $data,
    ]);
}

function encode($id, $connection = 'main')
{
    $config = config('hashids.connections.' . $connection);
    return (new \Hashids\Hashids($config['salt'], $config['length'], $config['alphabet']))->encode($id);
}

function decode($hash, $connection = 'main')
{
    $config = config('hashids.connections.' . $connection);
    $decoded = (new \Hashids\Hashids($config['salt'], $config['length'], $config['alphabet']))->decode($hash);
    if (count($decoded) > 0) {
        return $decoded[0];
    }
    return null;
}

function save_avatar(string $username, string $base64)
{
    save_image('avatars/' . $username, $base64);
}

function save_image(string $path_uri, string $base64)
{
    $path = public_path($path_uri);
    if (!File::exists($path)) {
        File::makeDirectory($path, 0777, true);
    }

    $img = Image::make($base64);
    $img->save($path . '/full.jpg');

    $thumbnail = $img->resize(150, null, function ($constraint) {
        $constraint->aspectRatio();
    });
    $thumbnail->save($path . '/thumbnail.jpg');
}

function combine_image(string $path_uri, string $filename, ...$base64_images)
{
    if (count($base64_images) > 0) {
        $img_canvas = Image::canvas(150 * count($base64_images), 200);
        foreach ($base64_images as $key => $base64) {
            $img = Image::make($base64)->resize(150, 200);
            if ($key == 0) {
                $img_canvas->insert($img);
            } else {
                $img_canvas->insert($img, 'top-right');
            }
        }
        $path = public_path($path_uri);
        if (!File::exists($path)) {
            File::makeDirectory($path, 0777, true);
        }
        $img_canvas->save($path . '/' . $filename);
    }
}

function send_verification(int $id, string $mobile, string $otp, string $text = 'Verification code from Panda Laundry '): bool
{
    $sent = false;
    try {
        $message = Nexmo::message()->send([
            'to' => str_replace('+', '', $mobile),
            'from' => '@pandalaundry',
            'text' => $text . $otp,
        ]);
        $sent = true;
        // $message['message-id'];
    } catch (\Throwable $th) {
        $r = new RejectedSMS;
        $r->user_id = $id;
        $r->mobile = $mobile;
        $r->type = 'otp';
        $r->message = 'Verification code from Panda Laundry ' . $otp;
        $r->save();
        Log::error($th);
    }
    return $sent;
}

function generateToken(User $user, string $type = 'customer')
{
    OauthAccessToken::where('user_id', $user->id)->delete();
    $data = [
        'token' => $user->createToken(config('app.name') . '-' . $type)->accessToken,
        'user' => new UserResource($user),
    ];

    return $data;
}

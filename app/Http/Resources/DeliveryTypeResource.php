<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class DeliveryTypeResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'hashid' => encode($this->id, 'model'),
            'name' => $this->name,
            'price_group' => $this->price_group,
            'hours' => $this->hours,
        ];
    }
}

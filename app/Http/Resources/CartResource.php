<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CartResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $payment_status = 'pending';
        if ($this->$payment_status == 1) {
            $payment_status = 'processing';
        } elseif ($this->$payment_status == 2) {
            $payment_status = 'success';
        } elseif ($this->$payment_status == 3) {
            $payment_status = 'declined';
        }
        $status = 'pending';
        if ($this->$status == 1) {
            $status = 'for_pickup';
        } elseif ($this->$status == 2) {
            $status = 'processing';
        } elseif ($this->$status == 3) {
            $status = 'for_delivery';
        } elseif ($this->$status == 4) {
            $status = 'completed';
        } elseif ($this->$status == 5) {
            $status = 'cancelled';
        } elseif ($this->$status == 6) {
            $status = 'declined';
        }

        return [
            'hashid' => encode($this->id, 'model'),
            'order_number' => encode($this->id, 'order'),
            'user' => new UserResource($this->user),
            'pickup_driver' => new UserResource($this->pickupDriver),
            'delivery_driver' => new UserResource($this->deliveryDriver),
            'address' => new AddressResource($this->address),
            'notes' => $this->notes,
            'sub_total' => $this->sub_total,
            'delivery_fee' => $this->delivery_fee,
            'delivery_type' => $this->delivery_type,
            'payment_method' => $this->payment_method,
            'created_at' => $this->created_at,
            'payment_status' => $payment_status,
            'status' => $status,
            'status_int' => $this->status,
            'items' => CartItemResource::collection($this->items),
        ];
    }
}

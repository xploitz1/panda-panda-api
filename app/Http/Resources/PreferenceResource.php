<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PreferenceResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'bleach' => $this->bleach == 1,
            'softener' => $this->softener == 1,
            'perfume' => $this->perfume == 1,
            'ironing' => $this->ironing,
            'starch' => $this->starch,
            'returned' => $this->returned,
        ];
    }
}

<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ClothesResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $prices = [
            'dry_clean' => 0,
            'iron' => 0,
            'wash_iron' => 0,
        ];
        $regular_prices = $prices;
        $express_prices = $prices;
        foreach ($this->prices as $item) {
            $prices = [
                'dry_clean' => $item->dry_clean,
                'iron' => $item->iron,
                'wash_iron' => $item->wash_iron,
            ];
            if ($item->delivery_type == 'regular') {
                $regular_prices = $prices;
            } elseif ($item->delivery_type == 'express') {
                $express_prices = $prices;
            }
        }
        return [
            'hashid' => encode($this->id, 'model'),
            'name' => $this->name,
            'slug' => $this->slug,
            'regular_prices' => $regular_prices,
            'express_prices' => $express_prices,
        ];
    }
}

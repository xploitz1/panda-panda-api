<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class AddressResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'hashid' => encode($this->id, 'model'),
            'lat' => $this->lat,
            'lng' => $this->lng,
            'street' => $this->street,
            'building' => $this->building,
            'floor' => $this->floor,
            'other' => $this->other,
            'mobile' => $this->mobile,
            'default' => $this->default == 1 ? true : false,
        ];
    }
}

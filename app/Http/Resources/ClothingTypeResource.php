<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ClothingTypeResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $items_arr = [];
        foreach ($this->items as $item) {
            array_push($items_arr, $item->name);
        }
        $items = implode(', ', $items_arr);
        return [
            'hashid' => encode($this->id, 'model'),
            'name' => $this->name,
            'title' => title_case($this->name) . ($this->name == 'other' ? 's' : '\'s Clothes'),
            'items' => $items,
        ];
    }
}

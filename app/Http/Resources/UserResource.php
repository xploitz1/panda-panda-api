<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'hashid' => encode($this->id, 'model'),
            'name' => $this->name,
            'username' => $this->username,
            'mobile' => $this->mobile,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'vehicle' => $this->vehicle() != null ? new VehicleResource($this->vehicle()) : null,
        ];
    }
}

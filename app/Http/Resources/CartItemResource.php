<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CartItemResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'hashid' => encode($this->id, 'model'),
            'cloth' => new ClothesResource($this->cloth),
            'process_type' => $this->process_type,
            'quantity' => $this->quantity,
            'price' => $this->price,
        ];
    }
}

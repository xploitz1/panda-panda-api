<?php

namespace App\Http\Controllers\API\Address;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Interfaces\AddressInterface;

class AddressController extends Controller
{
    protected $address;

    public function __construct(AddressInterface $address)
    {
        $this->address = $address;
    }

    public function list()
    {
        return $this->address->list(request());
    }

    public function add()
    {
        return $this->address->add(request());
    }

    public function update()
    {
        return $this->address->update(request());
    }

    public function remove()
    {
        return $this->address->remove(request());
    }

    public function getDefault()
    {
        return $this->address->getDefault(request());
    }
}

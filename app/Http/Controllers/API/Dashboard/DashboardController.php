<?php

namespace App\Http\Controllers\API\Dashboard;

use App\Http\Controllers\Controller;
use App\Interfaces\DashboardInterface;

class DashboardController extends Controller
{
    protected $dashboard;

    public function __construct(DashboardInterface $dashboard)
    {
        $this->dashboard = $dashboard;
    }

    public function summary()
    {
        return $this->dashboard->summary(request());
    }
}

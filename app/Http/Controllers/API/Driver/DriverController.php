<?php

namespace App\Http\Controllers\API\Driver;

use App\Http\Controllers\Controller;
use App\Interfaces\DriverInterface;

class DriverController extends Controller
{
    protected $driver;

    public function __construct(DriverInterface $driver)
    {
        $this->driver = $driver;
    }

    public function register()
    {
        return $this->driver->register(request());
    }

    public function verifyRegistrationOTP()
    {
        return $this->driver->verifyRegistrationOTP(request());
    }

    public function uploadFiles()
    {
        return $this->driver->uploadFiles(request());
    }

    public function getStatus()
    {
        return $this->driver->getStatus(request());
    }
}

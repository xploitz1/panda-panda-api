<?php

namespace App\Http\Controllers\API\User;

use App\Http\Controllers\Controller;
use App\Interfaces\UserInterface;

class UserController extends Controller
{
    protected $user;

    public function __construct(UserInterface $user)
    {
        return $this->user = $user;
    }

    public function list()
    {
        return $this->user->list(request());
    }

    public function details()
    {
        return $this->user->details(request());
    }

    public function update()
    {
        return $this->user->update(request());
    }

    public function changeAvatar()
    {
        return $this->user->changeAvatar(request());
    }

    public function changeMobile()
    {
        return $this->user->changeMobile(request());
    }

    public function toggleAdminRole()
    {
        return $this->user->toggleAdminRole(request());
    }

    public function toggleAdminStatus()
    {
        return $this->user->toggleAdminStatus(request());
    }

    public function toggleDriverStatus()
    {
        return $this->user->toggleDriverStatus(request());
    }
}

<?php

namespace App\Http\Controllers\API\Profile;

use App\Http\Controllers\Controller;
use App\Interfaces\ProfileInterface;

class ProfileController extends Controller
{
    protected $profile;

    public function __construct(ProfileInterface $profile)
    {
        $this->profile = $profile;
    }

    public function fetch()
    {
        return $this->profile->fetch(\request());
    }

    public function update()
    {
        return $this->profile->update(\request());
    }

    public function changeMobile()
    {
        return $this->profile->changeMobile(\request());
    }

    public function changeAvatar()
    {
        return $this->profile->changeAvatar(\request());
    }
}

<?php

namespace App\Http\Controllers\API\Cart;

use Illuminate\Http\Request;
use App\Interfaces\CartInterface;
use App\Http\Controllers\Controller;

class CartController extends Controller
{
    protected $cart;

    public function __construct(CartInterface $cart)
    {
        $this->cart = $cart;
    }

    public function myList()
    {
        return $this->cart->myList(request());
    }

    public function list()
    {
        return $this->cart->list(request());
    }

    public function add()
    {
        return $this->cart->add(request());
    }

    public function updatePaymentStatus()
    {
        return $this->cart->updatePaymentStatus(request());
    }

    public function updateOrderStatus()
    {
        return $this->cart->updateOrderStatus(request());
    }

    public function transit()
    {
        return $this->cart->transit(request());
    }
}

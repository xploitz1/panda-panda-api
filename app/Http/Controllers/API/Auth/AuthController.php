<?php

namespace App\Http\Controllers\API\Auth;

use Illuminate\Http\Request;
use App\Interfaces\AuthInterface;
use App\Http\Controllers\Controller;

class AuthController extends Controller
{
    protected $auth;

    public function __construct(AuthInterface $auth)
    {
        $this->auth = $auth;
    }

    public function requestOTP()
    {
        return $this->auth->requestOTP(request());
    }

    public function verifyOTP()
    {
        return $this->auth->verifyOTP(request());
    }

    public function refreshToken()
    {
        return $this->auth->refreshToken(request());
    }

    public function driverRequestOTP()
    {
        return $this->auth->driverRequestOTP(request());
    }

    public function adminRequestOTP()
    {
        return $this->auth->adminRequestOTP(request());
    }
}

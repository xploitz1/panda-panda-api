<?php

namespace App\Http\Controllers\API\Preference;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Interfaces\PreferenceInterface;

class PreferenceController extends Controller
{
    protected $pref;

    public function __construct(PreferenceInterface $pref)
    {
        $this->pref = $pref;
    }

    public function get()
    {
        return $this->pref->get(request());
    }

    public function update()
    {
        return $this->pref->update(request());
    }
}

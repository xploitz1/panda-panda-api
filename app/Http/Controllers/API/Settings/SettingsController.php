<?php

namespace App\Http\Controllers\API\Settings;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Interfaces\SettingsInterface;

class SettingsController extends Controller
{
    protected $settings;

    public function __construct(SettingsInterface $settings)
    {
        $this->settings = $settings;
    }

    public function get()
    {
        return $this->settings->get(request());
    }

    public function update()
    {
        return $this->settings->update(request());
    }
}

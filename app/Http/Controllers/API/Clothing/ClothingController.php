<?php

namespace App\Http\Controllers\API\Clothing;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Interfaces\ClothingInterface;

class ClothingController extends Controller
{
    protected $clothing;

    public function __construct(ClothingInterface $clothing)
    {
        $this->clothing = $clothing;
    }

    public function types()
    {
        return $this->clothing->types(request());
    }

    public function groups()
    {
        return $this->clothing->groups(request());
    }

    public function clothes()
    {
        return $this->clothing->clothes(request());
    }

    public function deliveryTypes()
    {
        return $this->clothing->deliveryTypes(request());
    }
}

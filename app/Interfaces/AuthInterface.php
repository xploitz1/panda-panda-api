<?php

namespace App\Interfaces;

interface AuthInterface
{
    public function requestOTP($req);

    public function verifyOTP($req);

    public function refreshToken($req);

    public function driverRequestOTP($req);

    public function adminRequestOTP($req);
}

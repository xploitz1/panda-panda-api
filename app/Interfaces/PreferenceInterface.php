<?php

namespace App\Interfaces;

interface PreferenceInterface
{
    public function get($req);

    public function update($req);
}

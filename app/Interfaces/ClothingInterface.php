<?php

namespace App\Interfaces;

interface ClothingInterface
{
    public function types($req);

    public function groups($req);

    public function clothes($req);

    public function deliveryTypes($req);
}

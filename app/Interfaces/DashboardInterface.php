<?php

namespace App\Interfaces;

interface DashboardInterface
{
    public function summary($req);
}

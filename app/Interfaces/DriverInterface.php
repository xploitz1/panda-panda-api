<?php

namespace App\Interfaces;

interface DriverInterface
{
    public function register($req);

    public function verifyRegistrationOTP($req);

    public function uploadFiles($req);

    public function getStatus($req);
}

<?php

namespace App\Interfaces;

interface CartInterface
{
    public function myList($req);

    public function list($req);

    public function add($req);

    public function updatePaymentStatus($req);

    public function updateOrderStatus($req);

    public function transit($req);
}

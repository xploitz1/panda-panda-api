<?php

namespace App\Interfaces;

interface ProfileInterface
{
    public function fetch($req);

    public function update($req);

    public function changeAvatar($req);

    public function changeMobile($req);
}

<?php

namespace App\Interfaces;

interface SettingsInterface
{
    public function get($req);

    public function update($req);
}

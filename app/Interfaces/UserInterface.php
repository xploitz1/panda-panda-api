<?php

namespace App\Interfaces;

interface UserInterface
{
    public function list($req);

    public function details($req);

    public function update($req);

    public function changeAvatar($req);

    public function changeMobile($req);

    public function toggleAdminRole($req);

    public function toggleAdminStatus($req);

    public function toggleDriverStatus($req);
}

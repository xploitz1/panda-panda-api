<?php

use App\Models\ClothingType;
use Illuminate\Database\Seeder;

class SeedClothingType extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $types = ['men', 'women', 'kid', 'other'];
        foreach ($types as $type) {
            ClothingType::firstOrCreate(['name' => $type]);
        }
    }
}

<?php

use App\Models\Cloth;
use App\Models\Pricing;
use Illuminate\Database\Seeder;

class SeedPrices extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $clothes = Cloth::where('status', 1)->get();
        foreach ($clothes as $cloth) {
            Pricing::firstOrCreate(['cloth_id' => $cloth->id, 'delivery_type' => 'express'], ['dry_clean' => 20, 'iron' => 9, 'wash_iron' => 14]);
            Pricing::firstOrCreate(['cloth_id' => $cloth->id, 'delivery_type' => 'regular'], ['dry_clean' => 10, 'iron' => 4.5, 'wash_iron' => 7]);
        }
    }
}

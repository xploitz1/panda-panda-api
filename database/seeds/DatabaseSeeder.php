<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(SeedClothingGroup::class);
        $this->call(SeedClothingType::class);
        $this->call(SeedDeliveryType::class);
        $this->call(SeedSettings::class);
        $this->call(SeedClothes::class);
        $this->call(SeedPrices::class);
    }
}

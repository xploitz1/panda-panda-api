<?php

use App\Models\DeliveryType;
use Illuminate\Database\Seeder;

class SeedDeliveryType extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DeliveryType::firstOrCreate(['name' => 'regular', 'price_group' => 'standard'], ['hours' => 24]);
        DeliveryType::firstOrCreate(['name' => 'express', 'price_group' => 'premium'], ['hours' => 4]);
    }
}

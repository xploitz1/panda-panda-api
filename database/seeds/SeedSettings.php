<?php

use App\Models\Settings;
use Illuminate\Database\Seeder;

class SeedSettings extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Settings::firstOrCreate(['name' => 'delivery_fee'], ['value' => 8]);
    }
}

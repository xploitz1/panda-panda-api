<?php

use App\Models\Cloth;
use Illuminate\Database\Seeder;

class SeedClothes extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $male_traditionals = [
            'Thawb/Dishdasha (Summer)',
            'Thawb/Dishdasha (Winter)',
            'Gutrah',
            'Shimagh',
            'Hijab/Scarf',
            'Daraa/Galabieh',
            'Daraa/Galabieh Two-piece',
            'Besht',
            'Besht (Winter)',
            'Punjabi',
        ];
        foreach ($male_traditionals as $value) {
            Cloth::firstOrCreate(['name' => $value, 'slug' => str_slug($value), 'type_id' => 1, 'group_id' => 1]);
        }
        $female_traditionals = [
            'Abaya',
            'Hijab/Scarf',
            'Daraa/Galabieh',
            'Daraa/Galabieh Two-piece',
            'Saree',
            'Besht',
            'Besht (Winter)',
            'Punjabi',
            'Niqab',
        ];
        foreach ($female_traditionals as $value) {
            Cloth::firstOrCreate(['name' => $value, 'slug' => str_slug($value), 'type_id' => 2, 'group_id' => 1]);
        }
        $kid_traditionals = [
            'Thawb/Dishdasha (Summer)',
            'Thawb/Dishdasha (Winter)',
            'Gutrah',
            'Shimagh',
            'Hijab/Scarf',
            'Daraa/Galabieh',
            'Daraa/Galabieh Two-piece',
            'Besht',
            'Besht (Winter)',
            'Punjabi',
            'Abaya',
            'Saree',
            'Niqab',
        ];
        foreach ($kid_traditionals as $value) {
            Cloth::firstOrCreate(['name' => $value, 'slug' => str_slug($value), 'type_id' => 3, 'group_id' => 1]);
        }

        $all_tops = [
            'Shirt',
            'Blouse',
            'Silk blouse',
            'Vest',
            'Cardigan/Jumper',
            'T-shirt',
            'Officer Uniform Shirt',
            'Sweater/Pullover',
        ];
        foreach ($all_tops as $value) {
            Cloth::firstOrCreate(['name' => $value, 'slug' => str_slug($value), 'type_id' => 1, 'group_id' => 2]);
            Cloth::firstOrCreate(['name' => $value, 'slug' => str_slug($value), 'type_id' => 2, 'group_id' => 2]);
            Cloth::firstOrCreate(['name' => $value, 'slug' => str_slug($value), 'type_id' => 3, 'group_id' => 2]);
        }

        $all_bottoms = [
            'Trousers/Pants',
            'Jeans',
            'Shorts',
            'Officer Uniform Trouser/Pants',
        ];
        foreach ($all_bottoms as $value) {
            Cloth::firstOrCreate(['name' => $value, 'slug' => str_slug($value), 'type_id' => 1, 'group_id' => 3]);
            Cloth::firstOrCreate(['name' => $value, 'slug' => str_slug($value), 'type_id' => 2, 'group_id' => 3]);
            Cloth::firstOrCreate(['name' => $value, 'slug' => str_slug($value), 'type_id' => 3, 'group_id' => 3]);
        }

        $all_undergarments = [
            'Socks',
            'Undershirt',
            'Sarwal Thawb',
            'Pyjamas',
            'Lungi/Wizar',
            'Underwear/Underpant',
        ];
        foreach ($all_undergarments as $value) {
            Cloth::firstOrCreate(['name' => $value, 'slug' => str_slug($value), 'type_id' => 1, 'group_id' => 4]);
            Cloth::firstOrCreate(['name' => $value, 'slug' => str_slug($value), 'type_id' => 2, 'group_id' => 4]);
            Cloth::firstOrCreate(['name' => $value, 'slug' => str_slug($value), 'type_id' => 3, 'group_id' => 4]);
        }
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDriverInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('driver_infos')) {
            Schema::create('driver_infos', function (Blueprint $table) {
                $table->id();
                $table->integer('user_id')->unsigned();
                $table->string('qid_number')->nullable()->default('');
                $table->text('qid_base64')->nullable();
                $table->text('qdl_base64')->nullable();
                $table->text('cv_base64')->nullable();
                $table->timestamps();
            });

            $drivers = \App\User::where('driver', 1)->get();
            foreach ($drivers as $driver) {
                $di = new \App\Models\DriverInfo;
                $di->user_id = $driver->id;
                $di->save();
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('driver_infos');
    }
}

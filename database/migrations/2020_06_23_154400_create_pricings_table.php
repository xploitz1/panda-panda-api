<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePricingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('pricings')) {
            Schema::create('pricings', function (Blueprint $table) {
                $table->id();
                $table->integer('cloth_id')->unsigned();
                $table->double('dry_clean');
                $table->double('iron');
                $table->double('wash_iron');
                $table->string('delivery_type');
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pricings');
    }
}

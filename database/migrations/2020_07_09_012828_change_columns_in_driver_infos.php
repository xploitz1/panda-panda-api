<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeColumnsInDriverInfos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasColumn('driver_infos', 'qid_base64')) {
            Schema::table('driver_infos', function (Blueprint $table) {
                $table->renameColumn('qid_base64', 'qid_front_base64');
                $table->text('qid_back_base64')->nullable();
            });
        }
        if (Schema::hasColumn('driver_infos', 'qdl_base64')) {
            Schema::table('driver_infos', function (Blueprint $table) {
                $table->renameColumn('qdl_base64', 'qdl_front_base64');
                $table->text('qdl_back_base64')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('driver_infos', function (Blueprint $table) {
            //
        });
    }
}

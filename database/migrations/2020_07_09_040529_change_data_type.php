<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeDataType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasColumns('driver_infos', ['qid_front_base64', 'qid_back_base64', 'qdl_front_base64', 'qdl_back_base64', 'cv_base64'])) {
            Schema::table('driver_infos', function (Blueprint $table) {
                $table->integer('qid_front_base64')->default(0)->change();
                $table->integer('qid_back_base64')->default(0)->change();
                $table->integer('qdl_front_base64')->default(0)->change();
                $table->integer('qdl_back_base64')->default(0)->change();
                $table->integer('cv_base64')->default(0)->change();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('driver_infos', function (Blueprint $table) {
            //
        });
    }
}

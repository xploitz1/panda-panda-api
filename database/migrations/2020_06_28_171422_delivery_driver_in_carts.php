<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DeliveryDriverInCarts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasColumn('carts', 'driver_id')) {
            Schema::table('carts', function (Blueprint $table) {
                $table->renameColumn('driver_id', 'pickup_driver_id');
            });
        }
        if (!Schema::hasColumn('carts', 'delivery_driver_id')) {
            Schema::table('carts', function (Blueprint $table) {
                $table->integer('delivery_driver_id')->unsigned();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('carts', function (Blueprint $table) {
            //
        });
    }
}
